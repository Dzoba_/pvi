const firstNotification = document.getElementById("bell_icon");
const secondNotification = document.getElementById("user_icon");

firstNotification.ondblclick = () => {
    firstNotification.animate(
    [
        { transform: "rotate(0)" }, 
        { transform: "rotate(-30deg)" },
        { transform: "rotate(30deg)"},
        { transform: "rotate(0)"}
    ],
        {
          duration: 500,
          iterations: 1,
        }
      );
}

firstNotification.onmouseover = () => {
    show(document.getElementById("bell-notifications"));
}

firstNotification.onmouseleave = () => {
    hide(document.getElementById("bell-notifications"));
}

secondNotification.onmouseover = () => {
    show(document.getElementById("user-notifications"));
}

secondNotification.onmouseleave = () => {
    hide(document.getElementById("user-notifications"));
}
function show(modalWindow) {
    modalWindow.classList.remove("hidden");
}

function hide(modalWindow) {
    modalWindow.classList.add("hidden");
}

function addStudentik() {
    show(document.getElementById("modalAdd"));
    editMode = false;
    editModalRows("PZ-21", "", "Male", "");
}

var editMode = false;
const saveBtn = document.getElementById("saveBtn");
saveBtn.onsubmit = saveStudentik;

function saveStudentik(form, e) {
    e.preventDefault();
    const formData = new FormData(form);
    var group = formData.get("group");
    var name = formData.get("name");
    var gender = formData.get("gender");
    var birthday = formData.get("birthday");
    if(editMode){
        editStudentRow(group, name, gender, birthday);
    } else {
        addStudentRow(group, name, gender, birthday);
    }
    hide(document.getElementById("modalAdd"));
}

function addStudentRow(group, name, gender, birthday){
    const trFirstStud = document.getElementById("trFirstStud");
    const studTable = document.getElementById("studTable");
    var newRow = studTable.insertRow(1);
    var trHtml = trFirstStud.innerHTML;
    trHtml = trHtml.replace("##grup",group );
    trHtml = trHtml.replace("##name",name );
    trHtml = trHtml.replace("##gender",gender);
    trHtml = trHtml.replace("##dob",birthday);
    newRow.innerHTML = trHtml;
}

var studentToDel;

function studentDel(buttonStudent) {
    studentToDel = buttonStudent.parentElement.parentElement;
    show(document.getElementById("deleteWarning"));
}

function dontDel() {
    hide(document.getElementById("deleteWarning"));
}

function realDelete() {
    hide(document.getElementById("deleteWarning"));
    hide(studentToDel);
}

function editModalRows(group, name, gender, birthday) {
    document.getElementById("group").value = group;
    document.getElementById("name").value = name;
    document.getElementById("gender").value = gender;
    document.getElementById("birthday").value = birthday;
}

var studentToEdit;

function studentEdit(butEdit) {
    studentToEdit = butEdit.parentElement.parentElement;
    show(document.getElementById("modalAdd"));
    var group = studentToEdit.cells[1].innerHTML;
    var name = studentToEdit.cells[2].innerHTML;
    var gender = studentToEdit.cells[3].innerHTML;
    var birthday = studentToEdit.cells[4].innerHTML;
    editModalRows(group, name, gender, birthday);
    editMode = true;
}

function editStudentRow(group, name, gender, birthday) {
    var trHtml = trFirstStud.innerHTML;
    trHtml = trHtml.replace("##grup",group );
    trHtml = trHtml.replace("##name",name );
    trHtml = trHtml.replace("##gender",gender);
    trHtml = trHtml.replace("##dob",birthday);
    studentToEdit.innerHTML = trHtml;
}